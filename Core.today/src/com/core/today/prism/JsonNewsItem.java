package com.core.today.prism;

import android.graphics.Bitmap;


class JsonNewsItem {
	private String keyword;
	private String article;
	private String event1;
	private String event2;
	private String event3;
//<<<<<<< HEAD
	private String image;
	private Bitmap bmImg;
//=======
	private String detail="See detail";
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
	public boolean isOpen;
	public boolean isClicked;
	
	/**
	 * Initialize with icon and data array
	 */
	

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItem(String keyword, String article, String image, String event1,String event2, String event3,Bitmap bmImg) {
		this.keyword = keyword;
		this.article =article;
		this.event1=event1;
		this.event2=event2;
		this.event3=event3;
		this.image = image;
		this.bmImg = bmImg;
		this.isOpen=false;
		this.isClicked=false;
		
	}
	

	
	public String getDetail(){
		return detail;
	}
	public String getTitle() {
		return keyword;
	}

	public void setTitle(String title) {
		this.keyword = title;
	}

	public String getLink() {
		return article;
	}

	public void setLink(String link) {
		this.article = link;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public Bitmap getbmImg() {
		return bmImg;
	}

	public void setbmImg(Bitmap bmImg) {
		this.bmImg=bmImg;
	}
	
	public boolean getisOpen(){
		return isOpen;
	}

	public void setisOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	public boolean getisClicked(){
		return isClicked;
	}

	public void setisClicked(boolean isClicked) {
		this.isClicked = isClicked;
	}
	public String getEvent1(){
		return event1;
	}
	
	public void setEvent1(String event1){
		this.event1=event1;
	}
	
	public String getEvent2(){
		return event2;
	}
	
	public void setEvent2(String event2){
		this.event2=event2;
	}
	
	public String getEvent3(){
		return event3;
	}
	
	public void setEvent3(String event3){
		this.event3=event3;
	}

	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItem other) {
		if (keyword.equals(other.getTitle())) {
			return false;
		} else if (article.equals(other.getLink())) {
			return false;
		} 
		
		return true;
	}

}
