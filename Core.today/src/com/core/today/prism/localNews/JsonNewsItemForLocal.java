package com.core.today.prism.localNews;

import java.util.ArrayList;
import java.util.Collections;


public class JsonNewsItemForLocal {
	private String title;
	private ArrayList<String> corp_name = new ArrayList<String>();
	private ArrayList<String> corp_url  = new ArrayList<String>();
	
//<<<<<<< HEAD
	
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
	
	/**
	 * Initialize with icon and data array
	 */
	

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItemForLocal(String title, ArrayList<String> corp_name, ArrayList<String> corp_url) {
		this.title = title;
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_name.add(corp_name.get(i));
			this.corp_url.add(corp_url.get(i));
		}
		
	}
	

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<String> getCorpName() {
		return corp_name;
	}

	public void setCorpName(ArrayList<String> corp_name) {
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_name.add(corp_name.get(i));
		}
	}
	
	public ArrayList<String> getCorpUrl() {
		return corp_url;
	}

	public void setCorp_Url(ArrayList<String> corp_url) {
		for(int i =0; i<corp_name.size();i++)
		{
			this.corp_url.add(corp_url.get(i));
		}
	}
	
	
	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItemForLocal other) {
		if (title.equals(other.getTitle())) {
			return false;
		} else if (corp_url.equals(other.getCorpUrl())) {
			return false;
		} 
		
		return true;
	}

}
