package com.core.today.prism;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView4 extends TextView{
	
	public CustomTextView4(Context context){
		super(context);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "DXCLEAR.ttf"));
	}
	
	public CustomTextView4(Context context, AttributeSet attrs){
		super(context, attrs);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "DXCLEAR.ttf"));
	}
	
	public CustomTextView4(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "DXCLEAR.ttf"));
	}
}