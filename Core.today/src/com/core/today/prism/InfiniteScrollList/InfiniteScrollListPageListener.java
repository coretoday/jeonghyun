package com.core.today.prism.InfiniteScrollList;

public interface InfiniteScrollListPageListener {
	public abstract void endOfList();
	public abstract void hasMore();
}
