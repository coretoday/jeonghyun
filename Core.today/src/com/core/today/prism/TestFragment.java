package com.core.today.prism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.core.today.prism.InfiniteScrollList.InfiniteScrollListPageListener;
import com.core.today.prism.categoryNews.CategoryItem;
import com.core.today.prism.eventlineNews.EventlineNews;
import com.core.today.prism.localNews.LocalNews;
import com.example.core.today.R;

import android.content.Context;
import android.content.Intent;
//<<<<<<< HEAD
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
//<<<<<<< HEAD


public final class TestFragment extends Fragment implements OnItemClickListener{
    private static final String KEY_CONTENT = "TestFragment:Content";
   private static final int REQUEST_CODE_ANOTHER = 000;
    private static String _content;
    int click_focus = -1; 
    int itemcount=0;

    private ArrayList<Integer> count_array = new ArrayList<Integer>();
    View toolbar;
    boolean blclickable = false;
    private Bitmap bmImg;
    Context context = null;
    Animation scaleUpAnimation;
    Animation scaleDownAnimation;
    JsonNewsAdapter adapter;
    static SlidingStopListView plv = null;
    String u="u";
    private String title;
    ScrollView mScrollview=null;
    private String group = null;

    
    Hashtable<String, String> ht;
    public static TestFragment newInstance(String title) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }


    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
           title = getArguments().getString("someTitle");
           
  
       // }
        
       
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       context = container.getContext(); // get Context

       //////////////////////////////////////////////////////////////////////////////////////////////////////////////
       StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       plv = (SlidingStopListView) LayoutInflater.from(context).inflate(
             R.layout.layout_listview_in_viewpager, container, false);
       plv.setVerticalScrollBarEnabled(false);
       String line;
       String page ="k";
       String topic="a",article="b",image="i", keyword1="c",keyword2="d",keyword3="e";
       //String group = null;

       try {
    	   group = "http://json.core.today/json2/?opt=2&o=1&c="+URLEncoder.encode(title,"UTF-8");
       } catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
       }
	

       JSONObject test;
       BufferedReader bufreader = null;
       HttpURLConnection urlConnection = null;
       JSONArray alljson;
       URL url1 = null;
       ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
       page = "";
       int count =0;
       try {

    	   String sample = group + "&n=20&sl=1";
          url1 = new URL(sample);
          urlConnection = (HttpURLConnection) url1.openConnection();

          bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));

			
			while((line=bufreader.readLine())!=null){
			   page+=line;
			}
           alljson = new JSONArray(page);

           for(int k =0; k < 20 ; count++){

            
                 test=new JSONObject(alljson.getString(count));

               // JSONArray keyw = new JSONArray(test.getString("k"));
                if(test.getString("k")!= "[]" && test.getString("nm") != "[]"){
                	try{
                      JSONArray arti = new JSONArray(test.getString("nm"));
                      JSONArray keywords = new JSONArray(test.getString("k"));
                      topic=test.getString("ti");
                      article=arti.getString(0);
                      count_array.add(count);
                      keyword1 = keywords.getString(0);
                      keyword2 = keywords.getString(1);
                      keyword3 = keywords.getString(2);
                     // Log.e("KEYWORD",keywords.getString(0));
                      image = test.getString("i");
                      m_orders.add(new JsonNewsItem(topic,article, image, keyword1, keyword2,keyword3,null));
                      k++;
                	}
                    catch(Exception e){

                    }
                }
  
          }
   
        }
        catch(Exception e){
     //Toast.makeText(context, "Fail to parse JSON", 1000).show();
        }
     /////////////////////////////////////////////////////////////////////////////////////////////parse JSON file

        urlConnection.disconnect();
     /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
     ///////////////////////////////////////////////////////////////////////////////add NewsItem to ArrayList

        adapter = new JsonNewsAdapter(context, R.layout.listitem , m_orders, new InfiniteScrollListPageListener(){

			@Override
			public void endOfList() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void hasMore() {
				// TODO Auto-generated method stub
				
			}
        	
        
        });
        plv.setAdapter(adapter);
        plv.setOnItemClickListener(this);

     //plv.setOnRefreshListener(TestFragment.this); /// set Refresh listener


        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        layout.setGravity(Gravity.CENTER);
        layout.addView(plv);
     /////////////////////////////////////////////////////////////////////////////inflate layout and listview
        //Log.e("TEST_MAIN", "PROBLEM IS " +plv.getAdapter().getCount());
        return layout;
    }
    
       public class NewsViewHolder{
          TextView keyword;
          TextView article;
          TextView event1;
          TextView event2;
          TextView event3;
          ImageView image;
          TextView detail;
          boolean isClicked;
          boolean isOpen;
       }
    
    /////////////////////////////////////////////////////////////////////////////////////////
       public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItem> {
          
    	  private ArrayList<JsonNewsItem> items;
    	  protected InfiniteScrollListPageListener infiniteListPageListener;
    	  
	      public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items,InfiniteScrollListPageListener infiniteListPageListener) {
	         super(context, textViewResourceId, items);
	         this.items = items;
	         this.infiniteListPageListener=infiniteListPageListener;
	      }

	      @Override
	      public View getView(final int position, View convertView, ViewGroup parent) {    
		      int count;
	          LayoutParams lp = null;
	          View v = convertView;
	          NewsViewHolder holder= null;
	          final SlidingStopListView listview = (SlidingStopListView) parent;
	          if (v == null) {      
		            LayoutInflater vi =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
		            v = vi.inflate(R.layout.listitem, null);
		            holder = new NewsViewHolder();
		            holder.keyword= (TextView) v.findViewById(R.id.dataItem01);
		            holder.article= (TextView) v.findViewById(R.id.dataItem02);
		            holder.event1 = (TextView) v.findViewById(R.id.event1);
		            holder.event2 = (TextView) v.findViewById(R.id.event2);
		            holder.event3 = (TextView) v.findViewById(R.id.event3);
		            holder.image = (ImageView) v.findViewById(R.id.news_image);
		            holder.detail = (TextView) v.findViewById(R.id.detail);
		            holder.isClicked=false;
		            holder.isOpen=false;
		            v.setTag(holder);
	          }
	          else
	          {
	        	  holder=(NewsViewHolder)v.getTag();
	          }
	        
	         final JsonNewsItem p = items.get(position);     
	         //v.setTag(holder);
	         
	         v.measure(MeasureSpec.UNSPECIFIED,MeasureSpec.UNSPECIFIED);
	         final View toolbar = v.findViewById(R.id.toolbar);
	         lp=(LayoutParams) toolbar.getLayoutParams();
	         lp.height = parent.getHeight()-v.getMeasuredHeight();
	         v.requestLayout();
         
	         Log.d("View Height","height: "+v.getHeight());
	         final LinearLayout back_layout = (LinearLayout) v.findViewById(R.id.back_layout);
	         
	         back_layout.setOnClickListener(new OnClickListener(){
	        	 @Override
	        	 public void onClick(View v) {
	        		 // TODO Auto-generated method stub
	        		 if(blclickable){
	        			 ItemSlidingUpTask task = new ItemSlidingUpTask();
	        			 SlidingTaskItem item = new SlidingTaskItem(listview,toolbar,position);
	        			 task.execute(item);
	        			 blclickable=false;
	        		 }
	        		 
	        		 //listview.setListViewScrollEnable();
	        		 p.setisOpen(false);
	        	 }
	         });
	         
		      if(holder.isOpen){
		         toolbar.setVisibility(View.GONE);
		         lp.bottomMargin=0- v.getHeight();
		         v.requestLayout();
		      }
		
		      if(p.isOpen){
		         toolbar.setVisibility(View.VISIBLE);
		         lp.bottomMargin=0;
		         v.requestLayout();
		      }
		
		     if(holder.isClicked){
		    	 holder.keyword.setTextColor(0xFF000000);
		     }
		     if(p.isClicked){
		    	 holder.keyword.setTextColor(0xFFA6A6A6);
		     }
		      if(p!=null){
		    	  
		         holder.keyword.setText(p.getTitle());
		         holder.isClicked = p.getisClicked();
		         holder.article.setText(p.getLink());
		         holder.event1.setText(p.getEvent1());
		         holder.event2.setText(p.getEvent2());
		         holder.event3.setText(p.getEvent3());
		         holder.detail.setText(p.getDetail());
		         holder.isOpen=p.getisOpen();
		         
		      }
      
		      holder.detail.setOnClickListener(new OnClickListener(){
		         @SuppressWarnings("null")
				@Override
		         public void onClick(View v) {
		        	 // TODO Auto-generated method stub	        	 
		            Intent detail_intent = new Intent(context,FullNews.class);
		            detail_intent.putExtra("news_url", group+"&n=20");
		            
		            detail_intent.putExtra("position", count_array.get(position));
		            try{
		            startActivity(detail_intent);
		            }
		            catch(Exception e){
		            	Toast.makeText(context, e.toString() + " intent 에러", Toast.LENGTH_LONG).show();
		            }
		         }
		      });
		     
		      holder.event1.setOnTouchListener(new OnTouchListener(){
		    	  @Override
		  		public boolean onTouch(View v, MotionEvent event) {
		  			int action = event.getAction();
		  			String query = p.getEvent1();
		  			if(action == MotionEvent.ACTION_DOWN){
		  				((TextView)v).setTextColor(0xFF353535);
		  			}
		  			else if(action == MotionEvent.ACTION_UP){
		  				((TextView)v).setTextColor(0xFFA6A6A6);
		  				Intent intent = new Intent(context, EventlineNews.class);
		  			    intent.putExtra("query_value", query);
		  			    startActivity(intent);
		  			}
		  			return true;
		  		}
		      });
		      holder.event2.setOnTouchListener(new OnTouchListener(){
		    	  @Override
		  		public boolean onTouch(View v, MotionEvent event) {
		  			int action = event.getAction();
		  			String query = p.getEvent2();
		  			if(action == MotionEvent.ACTION_DOWN){
		  				((TextView)v).setTextColor(0xFF353535);
		  			}
		  			else if(action == MotionEvent.ACTION_UP){
		  				((TextView)v).setTextColor(0xFFA6A6A6);
		  				Intent intent = new Intent(context, EventlineNews.class);
		  			    intent.putExtra("query_value", query);
		  			    startActivity(intent);
		  			}
		  			return true;
		  		}
		      });
		      holder.event3.setOnTouchListener(new OnTouchListener(){
		    	  @Override
		  		public boolean onTouch(View v, MotionEvent event) {
		  			int action = event.getAction();
		  			String query = p.getEvent3();
		  			if(action == MotionEvent.ACTION_DOWN){
		  				((TextView)v).setTextColor(0xFF353535);
		  			}
		  			else if(action == MotionEvent.ACTION_UP){
		  				((TextView)v).setTextColor(0xFFA6A6A6);
		  				Intent intent = new Intent(context, EventlineNews.class);
		  			    intent.putExtra("query_value", query);
		  			    startActivity(intent);
		  			}
		  			return true;
		  		}
		      });
		   
		      holder.detail.setText("관련 기사 모두 보기");
		      return v;   
	      } 
       }
         
        ///////////////////////////////////////////////////////////////////////listview adapter

           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position,
              long id) {
		        SlidingStopListView listview = (SlidingStopListView) parent;
		        toolbar= view.findViewById(R.id.toolbar);
		        NewsViewHolder holder = new NewsViewHolder();
		        JsonNewsItem p = adapter.getItem(position);
		        holder.image = (ImageView) view.findViewById(R.id.news_image);

		        //Toast.makeText(context, "item Height: " + view.getHeight()+" "+"Listview Height: "+parent.getHeight(), Toast.LENGTH_SHORT).show();
		        
		        if(p.isOpen)
		        {
		        	p.isOpen=false ; 
		        }
		        else{
		        	p.isOpen=true;
			        URL myFileUrl = null;   
	                if(p.getImage() != "null"){
	                	
		                try
		                {   
		                	myFileUrl = new URL(p.getImage());  
		                }   
		                catch(MalformedURLException e)
		                {   
			                // Todo Auto-generated catch block   
			                e.printStackTrace();   
		                }   
			            try  
			            {      
				                HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();   
				                conn.setDoInput(true);   
				                conn.connect();   
				                int length = conn.getContentLength();
				                InputStream is = conn.getInputStream(); 
				                       
				                bmImg = BitmapFactory.decodeStream(is);
				                   
				                p.setbmImg(resizeBitmapImageFn(bmImg,500));
				                holder.image.setImageBitmap(resizeBitmapImageFn(bmImg,500));  
				                    
			            }   
			            catch(IOException e)
			            {   
			            	e.printStackTrace();    
			            }
	                }
	                
	                listview.setListViewScrollDisable();
		        }
		        p.isClicked=true;

		        holder = new NewsViewHolder();
		        holder.keyword= (TextView) view.findViewById(R.id.dataItem01);
		        holder.article= (TextView) view.findViewById(R.id.dataItem02);
		        holder.event1 = (TextView) view.findViewById(R.id.event1);
		        holder.event2 = (TextView) view.findViewById(R.id.event2);
		        holder.event3 = (TextView) view.findViewById(R.id.event3);
		        holder.image = (ImageView) view.findViewById(R.id.news_image);
		        holder.detail = (TextView) view.findViewById(R.id.detail);
		        holder.isClicked=p.getisClicked();
		        holder.isOpen=p.getisOpen();
		        view.setTag(holder);
    
	    
	        // Creating the expand animation for the item
	           //ExpandAnimation expandAni = new ExpandAnimation(toolbar, 500);
	           //listview.smoothScrollToPositionFromTop(position, 0);
	           //toolbar.startAnimation(expandAni);
	        // Start the animation on the toolbar
		   // holder.keyword.setTextColor(0xFF0054FF);
		      holder.keyword.setTextColor(0xFFA6A6A6);

	        ItemSlidingDownTask task = new ItemSlidingDownTask();
	        SlidingTaskItem item = new SlidingTaskItem(listview,toolbar,position);
	        task.execute(item);

           }
           
           class SlidingTaskItem
           {
              public SlidingStopListView listview;
              public View toolbar;
              public int position;
              
              SlidingTaskItem(SlidingStopListView listview,View toolbar,int position)
              {
                 this.listview=listview;
                 this.toolbar=toolbar;
                 this.position=position;
              }
              
           }
           
           class ItemSlidingDownTask extends AsyncTask<SlidingTaskItem, Void, SlidingTaskItem> {
              @Override
              protected void onPreExecute(){
            	  //plv.setListViewScrollDisable();
                 ExpandAnimation expandAni = new ExpandAnimation(toolbar, 500);
                   toolbar.startAnimation(expandAni);
              }
       
        
                @Override
                protected void onPostExecute(SlidingTaskItem param) {
                    super.onPostExecute(param);
                    param.listview.smoothScrollToPositionFromTop(param.position, 0);
                    blclickable = true;
                }
        
   
                @Override
                protected SlidingTaskItem doInBackground(SlidingTaskItem... params) 
                {   
                		try 
                		{
                			Thread.sleep(450);
                		} catch (InterruptedException e) 
                		{
			                  // TODO Auto-generated catch block
			                  e.printStackTrace();
                		}
                    return params[0];
                }
            }
           
           class ItemSlidingUpTask extends AsyncTask<SlidingTaskItem, Void, SlidingTaskItem> {
               @Override
               protected void onPreExecute()
               {  
                  ExpandAnimation expandAni = new ExpandAnimation(toolbar, 500);
                    toolbar.startAnimation(expandAni);
               }
        
         
                 @Override
                 protected void onPostExecute(SlidingTaskItem param) 
                 {
                     super.onPostExecute(param);
                     param.listview.setListViewScrollEnable();
                     blclickable=true;
                     param.listview.smoothScrollToPositionFromTop(param.position, 0);
                 }
         
    
                 @Override
                 protected SlidingTaskItem doInBackground(SlidingTaskItem... params) {   
                    try 
                    {
                    	Thread.sleep(1000);
	                } catch (InterruptedException e)
	                {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
                    	return params[0];
                 }
             }

           public Bitmap resizeBitmapImageFn(Bitmap bmpSource, int maxResolution)
           { 
		              int iWidth = bmpSource.getWidth();      //비트맵이미�??�� ?��?��
		              int iHeight = bmpSource.getHeight();     //비트맵이미�??�� ?��?��
		              int newWidth = iWidth ;
		              int newHeight = iHeight ;
		              float rate = 0.0f;
		              //?��미�??�� �?�? ?���? 비율?�� 맞게 조절
		              if(iWidth > iHeight )
		              {
		                 if(maxResolution < iWidth )
		                 { 
		                    rate = maxResolution / (float) iWidth ; 
		                    newHeight = (int) (iHeight * rate); 
		                    newWidth = maxResolution; 
		                 }
		              }
		              else
		              {
		                 if(maxResolution < iHeight )
		                 {
		                    rate = maxResolution / (float) iHeight ; 
		                    newWidth = (int) (iWidth * rate);
		                    newHeight = maxResolution;
		                 }
		              }
		    
		              return Bitmap.createScaledBitmap(bmpSource, newWidth, newHeight, true); 
           }
           
    
}
