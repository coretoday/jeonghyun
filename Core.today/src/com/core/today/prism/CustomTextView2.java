package com.core.today.prism;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView2 extends TextView{
	
	public CustomTextView2(Context context){
		super(context);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt140.otf"));
	}
	
	public CustomTextView2(Context context, AttributeSet attrs){
		super(context, attrs);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt140.otf"));
	}
	
	public CustomTextView2(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		setTypeface(Typeface.createFromAsset(context.getAssets(), "THEjunggt140.otf"));
	}
}
